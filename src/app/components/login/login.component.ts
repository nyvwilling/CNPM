import { Component, OnInit } from '@angular/core';
import { UsercartService } from '../../services/usercart.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  usercartService;
  router;
  route;

  static get parameters() {
		return [UsercartService, Router, ActivatedRoute];
  }
  
  constructor(usercartService, router, route) { 
    this.usercartService = usercartService;
    this.router = router;
    this.route = route
  }

  ngOnInit() {
    let id = localStorage.getItem('currentUser');
    if(id){
      this.router.navigate(['/home']);
    }
  }
  login() {
    this.loading = true;
    let userData = {
      username: this.model.username,
      password: this.model.password
    }
    this.usercartService.signIn(userData).subscribe(res => {
      if(res.success){
        localStorage.setItem('currentUser', res.user._id);// lưu lại id
        this.router.navigate(["/home"]);
      }else{
        alert('Please check again!');
      }
    });
  }

}
