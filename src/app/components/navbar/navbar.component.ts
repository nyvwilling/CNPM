import { Component, OnInit } from '@angular/core';
import { UsercartService } from '../../services/usercart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  router;
  usercartService;
  name;

	static get parameters() {
		return [ Router, UsercartService ];
  }
  constructor(router, usercartService) { 
    this.router = router;
    this.usercartService = usercartService;
  }
  
  ngOnInit() {
    // get return url from route parameters
    let userID = localStorage.getItem('currentUser');// lấy biến được lưu trong localStorage
    if(userID){
      this.usercartService.getUserByID(userID).subscribe(res => {
        console.log(res);
        this.name = res.firstname+" "+res.lastname; 
      });
    }else{
      this.router.navigate(["/login"]);
      return false;
    }
  }
  logout(){
    //xóa biến lưu trong localStorage
    localStorage.clear();
    //quay lại trang login
    this.router.navigate(["/login"]);
  }

}
