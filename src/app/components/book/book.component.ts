import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../../services/book.service';
import { UsercartService } from '../../services/usercart.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

	route;
	book;
	bookId;
	bookService;
	router;
	usercartService;
	shoppingCart : any = [];
	id;
	tempCart : any = [];

	static get parameters() {
		return [ActivatedRoute, BookService, Router, UsercartService];
	}

	constructor(route, bookService, router, usercartService) {
		this.route = route;
		this.bookService = bookService;
		this.router = router;
		this.usercartService = usercartService;
	}

	ngOnInit() {  
		this.id = localStorage.getItem('currentUser');
		if(!this.id){
			return false;
		}else{
			this.route.params.subscribe(params => {
			this.bookId = params["id"];
			this.bookService.getBookById(this.bookId).subscribe(book => {
				this.book = book;
				console.log(book);
				});
				this.usercartService.getAllCarts(this.id).subscribe(cartList => {
					this.shoppingCart = cartList;
					console.log(this.shoppingCart, this.id);
				});
			});
		}
	}
	//kiểm tra tồn tại của sách trong giỏ hàng
	checkItemExists(item) {
		for(var a = 0; a < this.shoppingCart.length; a++) {
			if(this.shoppingCart[a].item == item.title) {
				return true;
			}
		}
		return false;
	}
	//tiến hành thêm sách vào giỏ
  addToCart() {
		console.log(this.shoppingCart);
		if(!this.checkItemExists(this.book)){// nếu món hàng không tồn tại trong giỏ trước đó
			if(this.shoppingCart.length !== 0){//nếu giỏ có sách trước đó
				this.tempCart = this.shoppingCart;//lấy tất cả sách đã có ở trước đó
				//thêm sách chưa tồn tại vào những vẫn giữ sách đã có trước đó
				let newItem = {
					item: this.book.title,
					quantity: 1,
					total: this.book.price
				}
				this.tempCart.push(newItem);
				//tiến hành khởi tạo biến lưu giá trị đã cập nhật và cập nhật lại csdl
				let userData = {
					id: this.id,
					shopcart: this.tempCart
				}
				this.usercartService.updateCart(userData).subscribe(res =>{
					alert(res.message);
					if(res.success){
						this.router.navigate(["/home"]);//nếu thành công thì quay lại home
					}
				});
			}else{//nếu giỏ trống
				let userData = {
					id: this.id,
					shopcart:[{
						item: this.book.title,
						quantity: 1,
						total: this.book.price
					}]
				}
				this.usercartService.updateCart(userData).subscribe(res =>{
					alert(res.message);
					if(res.success){
						this.router.navigate(["/home"]);
					}
				});
			}
		}else{
			this.tempCart = this.shoppingCart;
			for(var a = 0; a < this.tempCart.length; a++) {
				if(this.tempCart[a].item == this.book.title) {
					//cập nhật lại mảng tempcart
					this.tempCart[a].quantity += 1;
					this.tempCart[a].total += this.book.price;
				}
			}
			//tiến hành cập nhật
			let userData = {
				id: this.id,
				shopcart: this.tempCart 
			}
			//cập nhật
			this.usercartService.updateCart(userData).subscribe(res =>{
				alert(res.message);
				if(res.success){
					this.router.navigate(["/home"]);// nếu thành công thì quay lại home
				}else{
					alert('Error! Please try again!');
				}
			});
		}
	}
}
