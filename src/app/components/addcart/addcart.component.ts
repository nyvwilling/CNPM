import { Component, OnInit } from '@angular/core';
import { UsercartService } from '../../services/usercart.service';

@Component({
  selector: 'app-addcart',
  templateUrl: './addcart.component.html',
  styleUrls: ['./addcart.component.css']
})
export class AddcartComponent implements OnInit {

  shoppingCart: any = [];
  tempCart: any = [];
	usercartService;
  total: number = 0;
  id;

	static get parameters() {
		return [UsercartService];
	}

  constructor(usercartService) {
  	this.usercartService = usercartService;
  }

  ngOnInit() {
    this.id = localStorage.getItem('currentUser');
    this.usercartService.getAllCarts(this.id).subscribe(cartList => {
      this.shoppingCart = cartList;
      for(var a = 0; a < this.shoppingCart.length; a++) {
        this.total += this.shoppingCart[a].total;
      }
  	});
  }

  emptyCart() {
    let empty = {
      id: this.id,
      shopcart: []
    };
    this.usercartService.updateCart(empty).subscribe(result => {
      if(!result.success){
        alert("Could not clear cart!");
        return;
      }
    });
    alert('Clear cart is successfully!');
    this.shoppingCart = [];//xóa cart list
  }
  deleteCart(item){
    this.tempCart = this.shoppingCart;
    for(var index = 0; index < this.tempCart.length; index++) {
      if(this.tempCart[index].item === item) {
        this.tempCart.splice(index, 1);//loại bỏ phần tử có có id bị xóa khỏi list
      }
    }
    let temp = {
      id: this.id,
      shopcart: this.tempCart
    }
    this.usercartService.updateCart(temp).subscribe(result => {
  		if(result.success) {//sau khi xóa thành công thì cập nhật lại bookList trên browser
        this.shoppingCart = this.tempCart;
      } else {
        alert("Cart not successfully deleted");
      }
  	});
  }
}
