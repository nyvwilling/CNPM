#Yêu cầu
- Nodejs
- Mongodb

#Chạy project
- Bước 1: Git project về, khởi động mongodb
- Bước 2: Vào thư mục Server mở cmd hoặc Terminal lên chạy lệnh npm install, sau đó chạy lệnh node server để khởi động sever tại port 5000, có thể chỉnh port tại file server nếu đụng port
- Bước 3: Quay lại thư mục gốc chạy lệnh npm install, sau đó chạy lệnh npm install -g @angular/cli để cài đặt angular/cli
- Bước 4: Chạy lệnh ng serve để chạy project tại port 4200